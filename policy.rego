package terraform.analysis

import input as tfplan

resource_types = {"google_storage_bucket"}

default authz = false
authz {
    not touches_bucket
}

touches_bucket {
    all := resources["google_storage_bucket"]
    count(all) > 0
}

resources[resource_type] = all {
    some resource_type
    resource_types[resource_type]
    all := [name |
        name:= tfplan.resource_changes[_]
        name.type == resource_type
    ]
}