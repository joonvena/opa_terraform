unit:
	conftest test -p tests/unit/bucket.rego main.tf
	conftest test -p tests/unit/cloud_function.rego main.tf

integration:
	go test -v tests/integration/bucket_test.go

contract:
	terraform plan -out tfplan
	terraform show -json tfplan > tfplan.json
	conftest test -p tests/contract/test.rego tfplan.json