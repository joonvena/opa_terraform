package main

deny[msg] {
    not input.resource.google_cloudfunctions_function.function
    msg = "Define the cloud function resource"
}

deny[msg] {
    not input.resource.google_cloudfunctions_function.function.runtime
    msg = "Runtime environment is not defined"
}

deny[msg] {
    input.resource.google_cloudfunctions_function.function.runtime != "go113"
    msg = "Runtime environment should be go113"
}

deny[msg] {
    input.resource.google_cloudfunctions_function.function.available_memory_mb != 128
    msg = "Function should have available memory of 128mb"
}

deny[msg] {
    input.resource.google_cloudfunctions_function.function.trigger_http != true
    msg = "HTTP trigger should be enabled"
}

deny [msg] {
    input.resource.google_cloudfunctions_function_iam_member.invoker.member != "allUsers"
    msg = "Function should be public and available to everyone"
}