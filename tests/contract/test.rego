package main

deny[msg] {
    storage_object := [r | r := input.planned_values.root_module.resources[_]; r.address == "google_storage_bucket_object.archive"]
    storage_object[0].values.bucket != "opa-test-bucket"
    msg = "Bucket name should be opa-test-bucket"
}