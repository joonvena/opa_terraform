module terraform_tests

go 1.13

require (
	cloud.google.com/go/firestore v1.4.0 // indirect
	cloud.google.com/go/storage v1.12.0
	github.com/gruntwork-io/terratest v0.31.2
	github.com/stretchr/testify v1.6.1 // indirect
)
