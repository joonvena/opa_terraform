#!/bin/sh

OPA_OUTPUT=$(./opa eval --format pretty --data policy.rego --input tfplan.json "data.terraform.analysis.authz")

if [ "$OPA_OUTPUT" == "true" ]; then
    terraform apply --auto-approve
    exit 0
else
    exit 1
fi
