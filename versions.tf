terraform {
  required_providers {
    aiven = {
      source = "aiven/aiven"
      version = "2.1.16"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.5.0"
    }
    google = {
      source = "hashicorp/google"
      version = "3.51.0"
    }
  }
}
